/**
 * Created by eugene on 20.05.15.
 */

'use strict';

angular.module('notifications').factory('Notifications', ['$resource',
  function($resource) {
    return $resource('notifications/:notificationId', {
      notificationId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
