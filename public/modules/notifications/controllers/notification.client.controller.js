/**
 * Created by eugene on 20.05.15.
 */

'use strict';

angular.module('notifications').controller('NotificationController', ['$scope', 'lodash', '$stateParams', '$location', 'Authentication', 'Notifications', 'Unions',
  function ($scope, lodash, $stateParams, $location, Authentication, Notifications, Unions) {
    $scope.authentication = Authentication;

    $scope.init = function() {
      $scope.unions = Unions.query(null, function(unions) {
        $scope.unions = [];
        for (var i = 0; i < unions.length; i++) {
          var union = unions[i];
          if ($scope.notification != null && $scope.notification.unions.indexOf(union._id) > -1) {
            union.selected = true;
          }
          if (!union.parent) {
            union.children = buildSubtree(union, unions);
            $scope.unions.push(union);
          }
        }
      });
    };

    function buildSubtree(node, unions) {
      var result = [];
      for (var i = 0; i < unions.length; i++) {
        if (node._id == unions[i].parent) {
          result.push(unions[i]);
        }
      }
      for (i = 0; i < result.length; i++) {
        result[i].children = buildSubtree(result[i], unions);
      }
      return result;
    }

    $scope.fetch = function() {
      $scope.notifications = Notifications.query();
    };

    $scope.create = function() {
      function populateUnions(myUnions, rootUnion) {
        if (rootUnion.selected) {
          myUnions.push(rootUnion._id);
        }
        for (var i in rootUnion.children) {
          populateUnions(myUnions, rootUnion.children[i]);
        }
      }
      var myUnions = [];
      populateUnions(myUnions, this.unions[0]);
      var notification = new Notifications({
        title: this.title,
        content: this.content,
        unions: myUnions
      });
      notification.$save(function() {
        $location.path('notifications');

      }, function(errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    $scope.updateUnionSelection = function(union) {
      union.selected = !union.selected;
      for (var i in union.children) {
        $scope.updateUnionSelection(union.children[i]);
      }
    };

  }]);
