/**
 * Created by eugene on 20.05.15.
 */

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('notifications', ['ui.tree', 'daterangepicker', 'ngLodash']);
