/**
 * Created by eugene on 20.05.15.
 */

'use strict';

// Setting up route
angular.module('notifications').config(['$stateProvider',
  function($stateProvider) {
    // notifications state routing
    $stateProvider.
      state('notifications', {
        url: '/notifications',
        templateUrl: 'modules/notifications/views/main.client.view.html'
      }).
      state('new-notification', {
        url: '/notifications/create',
        templateUrl: 'modules/notifications/views/new.client.view.html'
      });
  }
]);
