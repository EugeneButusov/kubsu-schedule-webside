/**
 * Created by eugene on 20.05.15.
 */

'use strict';

angular.module('notifications').run(['Menus',
  function(Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', ' Новости', 'notifications');
  }
]);
