/**
 * Created by eugene on 14.11.14.
 */

'use strict';

angular.module('entity-managing').run(['Menus',
    function(Menus) {
        // Set top bar menu items
        Menus.addMenuItem('topbar', 'Управление', 'entity-managing', 'dropdown', null, null, ['admin']);
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Пользователи', 'accounts');
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Объединения', 'unions');
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Занятия', 'lessons');
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Площадки', 'playgrounds');
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Типы занятий', 'lesson-types');
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Предметы', 'subjects');
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Конфигурации системы', 'configurations');
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Занятия по расписанию', 'schedules');
    }
]);
