/**
 * Created by eugene on 14.11.14.
 */

'use strict';

// Setting up route
angular.module('entity-managing').config(['$stateProvider',
    function($stateProvider) {
        // Lessons state routing
        $stateProvider.
            state('listLessons', {
                url: '/lessons',
                templateUrl: 'modules/entity-managing/views/lessons/list.client.view.html'
            }).
            state('createLesson', {
                url: '/lessons/create',
                templateUrl: 'modules/entity-managing/views/lessons/new.client.view.html'
            }).
            state('editLesson', {
                url: '/lessons/:lessonId/edit',
                templateUrl: 'modules/entity-managing/views/lessons/edit.client.view.html'
            });

        //Playgrounds state routing
        $stateProvider.
            state('listPlaygrounds', {
                url: '/playgrounds',
                templateUrl: 'modules/entity-managing/views/playgrounds/list.client.view.html'
            }).
            state('createPlayground', {
                url: '/playgrounds/create',
                templateUrl: 'modules/entity-managing/views/playgrounds/new.client.view.html'
            }).
            state('editPlayground', {
                url: '/playgrounds/:playgroundId/edit',
                templateUrl: 'modules/entity-managing/views/playgrounds/edit.client.view.html'
            });

        //Unions state routing
        $stateProvider.
            state('listUnions', {
                url: '/unions',
                templateUrl: 'modules/entity-managing/views/unions/treelist.client.view.html'
            });

        //Subjects state config
        $stateProvider.
            state('listSubjects', {
                url: '/subjects',
                templateUrl: 'modules/entity-managing/views/subjects/list.client.view.html'
            }).
            state('createSubject', {
                url: '/subjects/create',
                templateUrl: 'modules/entity-managing/views/subjects/new.client.view.html'
            }).
            state('editSubject', {
                url: '/subjects/:subjectId/edit',
                templateUrl: 'modules/entity-managing/views/subjects/edit.client.view.html'
            });

        //Lesson types state config
        $stateProvider.
            state('listLessonTypes', {
                url: '/lesson-types',
                templateUrl: 'modules/entity-managing/views/lesson-types/list.client.view.html'
            }).
            state('createLessonType', {
                url: '/lesson-types/create',
                templateUrl: 'modules/entity-managing/views/lesson-types/new.client.view.html'
            }).
            state('editLessonType', {
                url: '/lesson-types/:lessonTypeId/edit',
                templateUrl: 'modules/entity-managing/views/lesson-types/edit.client.view.html'
            });

        //app-configurations state config
        $stateProvider.
          state('listConfigurations', {
              url: '/configurations',
              templateUrl: 'modules/entity-managing/views/configurations/list.client.view.html'
          }).
          state('createConfiguration', {
              url: '/configurations/create',
              templateUrl: 'modules/entity-managing/views/configurations/new.client.view.html'
          }).
          state('editConfiguration', {
              url: '/configurations/:configurationId/edit',
              templateUrl: 'modules/entity-managing/views/configurations/edit.client.view.html'
          });

        $stateProvider.
            state('listAccounts', {
                url: '/accounts',
                templateUrl: 'modules/entity-managing/views/accounts/list.client.view.html'
            });
        $stateProvider.
            state('updateAccount', {
              url: '/accounts/:accountId/edit',
              templateUrl: 'modules/entity-managing/views/accounts/edit.client.view.html'
            });
        $stateProvider.
          state('createSchedule', {
            url: '/schedules/create',
            templateUrl: 'modules/entity-managing/views/schedules/new.client.view.html'
          }).
          state('editSchedule', {
              url: '/schedules/:scheduleId/edit',
              templateUrl: 'modules/entity-managing/views/schedules/edit.client.view.html'
          })
          .state('listSchedules', {
              url: '/schedules',
              templateUrl: 'modules/entity-managing/views/schedules/list.client.view.html'
          });
    }
]);
