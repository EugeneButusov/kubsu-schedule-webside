/**
 * Created by eugene on 15.11.14.
 */

'use strict';

//Lessons service used for communicating with the articles REST endpoints
angular.module('entity-managing').factory('Lessons', ['$resource',
    function($resource) {
        return $resource('lessons/:lessonId', {
            lessonId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);