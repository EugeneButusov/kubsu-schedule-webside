/**
 * Created by eugene on 23.11.14.
 */

'use strict';

//Unions service used for communicating with the articles REST endpoints
angular.module('entity-managing').factory('Unions', ['$resource',
    function($resource) {
        return $resource('unions/:unionId', {
            unionId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);