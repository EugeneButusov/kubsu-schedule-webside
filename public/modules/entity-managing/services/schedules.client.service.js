/**
 * Created by eugene on 10.03.15.
 */

'use strict';

angular.module('entity-managing').factory('Schedules', ['$resource',
  function($resource) {
    return $resource('schedules/:scheduleId', {
      scheduleId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
