/**
 * Created by eugene on 29.04.15.
 */

'use strict';

//AppConfiguration service used for communicating with the articles REST endpoints
angular.module('entity-managing').factory('AppConfigurations', ['$resource',
  function($resource) {
    return $resource('configurations/:configurationId', {
      configurationId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
