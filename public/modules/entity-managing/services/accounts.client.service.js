/**
 * Created by eugene on 01.12.14.
 */

'use strict';

angular.module('entity-managing').factory('Accounts', ['$resource',
    function($resource) {
        return $resource('accounts/:accountId', {
            accountId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
