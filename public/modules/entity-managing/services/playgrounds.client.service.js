/**
 * Created by eugene on 15.11.14.
 */
'use strict';

//Playgrounds service used for communicating with the articles REST endpoints
angular.module('entity-managing').factory('Playgrounds', ['$resource',
    function($resource) {
        return $resource('playgrounds/:playgroundId', {
            playgroundId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);