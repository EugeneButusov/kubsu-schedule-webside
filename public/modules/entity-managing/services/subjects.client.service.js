/**
 * Created by eugene on 24.11.14.
 */

'use strict';

//Lessons service used for communicating with the articles REST endpoints
angular.module('entity-managing').factory('Subjects', ['$resource',
    function($resource) {
        return $resource('subjects/:subjectId', {
            subjectId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);