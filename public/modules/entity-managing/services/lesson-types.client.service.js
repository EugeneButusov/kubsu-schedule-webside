/**
 * Created by eugene on 28.11.14.
 */

'use strict';

//Lesson types service used for communicating with the articles REST endpoints
angular.module('entity-managing').factory('LessonTypes', ['$resource',
    function($resource) {
        return $resource('lesson-types/:lessonTypeId', {
            lessonTypeId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
