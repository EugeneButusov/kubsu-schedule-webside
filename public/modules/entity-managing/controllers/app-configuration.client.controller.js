/**
 * Created by eugene on 29.04.15.
 */

'use strict';

angular.module('entity-managing').controller('AppConfigurationController', ['$scope', '$stateParams', '$location', 'Authentication', 'AppConfigurations',
  function ($scope, $stateParams, $location, Authentication, AppConfigurations) {
    $scope.authentication = Authentication;

    $scope.find = function() {
      $scope.configurations = AppConfigurations.query();
    };

    $scope.create = function() {
      var dateComponents = this.scheduleCycle.startDate.split('-');
      var freeDays = this.scheduleCycle.freeDays.split(', ');
      for (var i in freeDays) {
        freeDays[i] = +freeDays[i];
      }
      var configuration = new AppConfigurations({
        name: this.name,
        active: this.active,
        scheduleCycle: {
          length: this.length,
          freeDays: freeDays,
          startDate: new Date(dateComponents[2], dateComponents[1] - 1, dateComponents[0])
        }
      });
      configuration.$save(function() {
        $location.path('configurations');

      }, function(errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    $scope.findOne = function() {
      $scope.configuration = AppConfigurations.get({
        configurationId: $stateParams.configurationId
      });
    };

    $scope.remove = function (configuration) {
      if (configuration) {
        configuration.$remove(function() {
          for (var i in $scope.configurations) {
            if ($scope.configurations[i] === configuration) {
              $scope.configurations.splice(i, 1);
            }
          }
        }, function(error) {
          alert(error.data.message);
        });
      } else {
        $scope.configuration.$remove(function() {
          $location.path('configurations');
        });
      }
    };

    $scope.update = function() {
      var configuration = $scope.configuration;
      var dateComponents = configuration.scheduleCycle.startDate.split('-');
      var freeDays = configuration.scheduleCycle.freeDays.split(', ');
      delete configuration.scheduleCycle.freeDays;
      for (var i in freeDays) {
        freeDays[i] = +freeDays[i];
      }
      configuration.scheduleCycle.startDate = new Date(dateComponents[2], dateComponents[1] - 1, dateComponents[0]);
      configuration.scheduleCycle.freeDays = freeDays;
      configuration.$update(function() {
        $location.path('configurations');
      }, function(errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };
  }]);
