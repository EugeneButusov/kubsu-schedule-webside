/**
 * Created by eugene on 23.11.14.
 */

'use strict';

angular.module('entity-managing').controller('UnionController', ['$scope', '$stateParams', '$location', 'Authentication', 'Unions',
    function ($scope, $stateParams, $location, Authentication, Unions) {
        $scope.authentication = Authentication;

        $scope.find = function() {
            $scope.unions = Unions.query(null, function(unions) {
                $scope.unions = [];
                for (var i = 0; i < unions.length; i++) {
                    var union = unions[i];
                    if (!union.parent) {
                        union.children = buildSubtree(union, unions);
                        $scope.unions.push(union);
                    }
                }
            });
        };

        function buildSubtree(node, unions) {
            var result = [];
            for (var i = 0; i < unions.length; i++) {
                if (node._id == unions[i].parent) {
                    result.push(unions[i]);
                }
            }
            for (i = 0; i < result.length; i++) {
                result[i].children = buildSubtree(result[i], unions);
            }
            return result;
        }

        $scope.create = function(parent) {
            var union = new Unions({
                name: 'Новое объединение: ' + (new Date()).toString(),
                parent:parent._id
            });
            union.$save(function(union) {
                if (!parent.children) {
                    parent.children = [];
                }
                parent.children.push(union);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.findOne = function() {
            $scope.union = Unions.get({
                unionId: $stateParams.unionId
            });
        };

        function findParentNode(node, currentNode) {
            if (node.parent == currentNode._id) {
                return currentNode;
            }
            else {
                for (var i = 0; i < currentNode.children.length; i++) {
                    var temp = findParentNode(node, currentNode.children[i]);
                    if (temp) {
                        return temp;
                    }
                }
                return null;
            }
        }

        $scope.drop = function (union) {
            union.$remove();
            var parentNode = null;
            for (var i = 0; i < $scope.unions.length; i++) {
                if ((parentNode = findParentNode(union, $scope.unions[i]))) {
                    break;
                }
            }

            for (i in parentNode.children) {
                if (parentNode.children[i] === union) {
                    parentNode.children.splice(i, 1);
                }
            }
        };

        $scope.beginUpdate = function(union) {
            union.editing = true;
        };
        $scope.cancelEditing = function(union) {
            union.editing = false;
        };

        $scope.save = function(union) {
            union.$update(function(union) {
                union.editing = false;
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
    }]);