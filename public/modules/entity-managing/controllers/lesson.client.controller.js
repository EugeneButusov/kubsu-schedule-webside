/**
 * Created by eugene on 14.11.14.
 */

'use strict';

angular.module('entity-managing').controller('LessonController', ['$scope', '$stateParams', '$location', 'Authentication', 'Lessons',
    function ($scope, $stateParams, $location, Authentication, Lessons) {
        $scope.authentication = Authentication;

        $scope.find = function() {
            $scope.lessons = Lessons.query();
        };

        $scope.create = function() {
            var lesson = new Lessons({
                number: this.number,
                beginTime: new Date(1970, 0, 1, this.beginTime.split(':')[0], this.beginTime.split(':')[1]),
                endTime: new Date(1970, 0, 1, this.endTime.split(':')[0], this.endTime.split(':')[1])
            });
            lesson.$save(function() {
                $location.path('lessons');

            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.findOne = function() {
            $scope.lesson = Lessons.get({
                lessonId: $stateParams.lessonId
            });
        };

        $scope.remove = function (lesson) {
            if (lesson) {
                lesson.$remove();

                for (var i in $scope.lessons) {
                    if ($scope.lessons[i] === lesson) {
                        $scope.lessons.splice(i, 1);
                    }
                }
            } else {
                $scope.lesson.$remove(function() {
                    $location.path('lessons');
                });
            }
        };

        $scope.update = function() {
            var lesson = $scope.lesson;
            lesson.beginTime =  new Date(1970, 0, 1, this.beginTime.split(':')[0], this.beginTime.split(':')[1]);
            lesson.endTime = new Date(1970, 0, 1, this.endTime.split(':')[0], this.endTime.split(':')[1]);

            lesson.$update(function() {
                $location.path('lessons');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

    }]);