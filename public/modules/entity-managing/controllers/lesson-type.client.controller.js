/**
 * Created by eugene on 28.11.14.
 */

'use strict';

angular.module('entity-managing').controller('LessonTypeController', ['$scope', '$stateParams', '$location', 'Authentication', 'LessonTypes',
    function ($scope, $stateParams, $location, Authentication, LessonTypes) {
        $scope.authentication = Authentication;

        $scope.find = function() {
            $scope.lessonTypes = LessonTypes.query();
        };

        $scope.create = function() {
            var lessonType = new LessonTypes({
                title: this.title
            });
            lessonType.$save(function() {
                $location.path('lesson-types');

            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.findOne = function() {
            $scope.lessonType = LessonTypes.get({
                lessonTypeId: $stateParams.lessonTypeId
            });
        };

        $scope.remove = function (lessonType) {
            if (lessonType) {
                lessonType.$remove();

                for (var i in $scope.lessonTypes) {
                    if ($scope.lessonTypes[i] === lessonType) {
                        $scope.lessonTypes.splice(i, 1);
                    }
                }
            } else {
                $scope.lessonType.$remove(function() {
                    $location.path('lesson-types');
                });
            }
        };

        $scope.update = function() {
            var lessonType = $scope.lessonType;

            lessonType.$update(function() {
                $location.path('lesson-types');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

    }]);
