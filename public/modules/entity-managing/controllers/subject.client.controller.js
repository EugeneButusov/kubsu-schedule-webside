/**
 * Created by eugene on 24.11.14.
 */

'use strict';

angular.module('entity-managing').controller('SubjectController', ['$scope', '$stateParams', '$location', 'Authentication', 'Subjects',
    function ($scope, $stateParams, $location, Authentication, Subjects) {
        $scope.authentication = Authentication;

        $scope.find = function() {
            $scope.subjects = Subjects.query();
        };

        $scope.create = function() {
            var subject = new Subjects({
                title: this.title,
                description: this.description
            });
            subject.$save(function() {
                $location.path('subjects');

            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.findOne = function() {
            $scope.subject = Subjects.get({
                subjectId: $stateParams.subjectId
            });
        };

        $scope.remove = function (subject) {
            if (subject) {
                subject.$remove();

                for (var i in $scope.subjects) {
                    if ($scope.subjects[i] === subject) {
                        $scope.subjects.splice(i, 1);
                    }
                }
            } else {
                $scope.subject.$remove(function() {
                    $location.path('subjects');
                });
            }
        };

        $scope.update = function() {
            var subject = $scope.subject;

            subject.$update(function() {
                $location.path('subjects');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

    }]);