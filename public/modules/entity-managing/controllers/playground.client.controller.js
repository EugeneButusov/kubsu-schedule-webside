/**
 * Created by eugene on 15.11.14.
 */

'use strict';

angular.module('entity-managing').controller('PlaygroundController', ['$scope', '$stateParams', '$location', 'Authentication', 'Playgrounds',
    function ($scope, $stateParams, $location, Authentication, Playgrounds) {
        $scope.authentication = Authentication;

        $scope.find = function() {
            $scope.playgrounds = Playgrounds.query();
        };

        $scope.create = function() {
            var playground = new Playgrounds({
                name: this.name
            });
            playground.$save(function() {
                $location.path('playgrounds');

            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.findOne = function() {
            $scope.playground = Playgrounds.get({
                playgroundId: $stateParams.playgroundId
            });
        };

        $scope.remove = function (playground) {
            if (playground) {
                playground.$remove();

                for (var i in $scope.playgrounds) {
                    if ($scope.playgrounds[i] === playground) {
                        $scope.playgrounds.splice(i, 1);
                    }
                }
            } else {
                $scope.playground.$remove(function() {
                    $location.path('playgrounds');
                });
            }
        };

        $scope.update = function() {
            var playground = $scope.playground;

            playground.$update(function() {
                $location.path('playgrounds');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

    }]);