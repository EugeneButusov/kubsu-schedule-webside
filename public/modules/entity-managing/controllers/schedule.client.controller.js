/**
 * Created by eugene on 10.03.15.
 */

'use strict';

angular.module('entity-managing')
  .controller('ScheduleController',
  ['$scope', '$stateParams', '$location', 'Authentication', 'Schedules', 'Subjects', 'LessonTypes', 'Lessons',
    'Accounts', 'Unions', 'Playgrounds',
  function ($scope, $stateParams, $location, Authentication, Schedules, Subjects, LessonTypes, Lessons, Accounts,
            Unions, Playgrounds) {
    $scope.authentication = Authentication;

    $scope.prepare = function() {
      $scope.subjects = Subjects.query();
      $scope.lessonTypes = LessonTypes.query();
      $scope.playgrounds = Playgrounds.query();
      $scope.lessons = Lessons.query();
      Accounts.query({kind: 'teacher'}, function(users) {
        $scope.teachers = users;
      });
      $scope.learningUnions = Unions.query(null, function(unions) {
        $scope.learningUnions = [];
        for (var i = 0; i < unions.length; i++) {
          var union = unions[i];
          if (!union.parent) {
            union.children = buildSubtree(union, unions);
            $scope.learningUnions.push(union);
          }
        }
      });
    };

    function buildSubtree(node, unions) {
      var result = [];
      for (var i = 0; i < unions.length; i++) {
        if (node._id == unions[i].parent) {
          result.push(unions[i]);
        }
      }
      for (i = 0; i < result.length; i++) {
        result[i].children = buildSubtree(result[i], unions);
      }
      return result;
    }

    $scope.find = function() {
      $scope.schedules = Schedules.query();
    };


    $scope.create = function() {
      function populateUnions(myUnions, rootUnion) {
        if (rootUnion.selected) {
          myUnions.push(rootUnion._id);
        }
        for (var i in rootUnion.children) {
          populateUnions(myUnions, rootUnion.children[i]);
        }
      }
      var myLearningUnions = [];
      populateUnions(myLearningUnions, this.learningUnions[0]);
      var schedule = new Schedules({
        cyclePosition: this.cyclePosition,
        subject: this.subject._id,
        type: this.type,
        lesson: this.lesson,
        teacher: this.teacher,
        playground: this.playground,
        learningUnions: myLearningUnions
      });
      schedule.$save(function() {
        $location.path('schedules');

      }, function(errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    $scope.findOne = function() {
      $scope.subjects = Subjects.query();
      $scope.lessonTypes = LessonTypes.query();
      $scope.playgrounds = Playgrounds.query();
      $scope.lessons = Lessons.query();
      $scope.schedule = Schedules.get({
        scheduleId: $stateParams.scheduleId
      });
      Accounts.query({kind: 'teacher'}, function(users) {
        $scope.teachers = users;
      });
      $scope.learningUnions = Unions.query(null, function(unions) {
        $scope.learningUnions = [];
        for (var i = 0; i < unions.length; i++) {
          var union = unions[i];
          if ($scope.schedule.learningUnions.indexOf(union._id) > -1) {
            union.selected = true;
          }
          if (!union.parent) {
            union.children = buildSubtree(union, unions);
            $scope.learningUnions.push(union);
          }
        }
      });
    };

    $scope.remove = function (schedule) {
      if (schedule) {
        schedule.$remove();

        for (var i in $scope.schedules) {
          if ($scope.schedules[i] === schedules) {
            $scope.schedule.splice(i, 1);
          }
        }
      } else {
        $scope.schedule.$remove(function() {
          $location.path('schedules');
        });
      }
    };


    $scope.updateUnionSelection = function(union) {
      union.selected = !union.selected;
      for (var i in union.children) {
        $scope.updateUnionSelection(union.children[i]);
      }
    };

    $scope.update = function() {
      var schedule = $scope.schedule;

      schedule.$update(function() {
        $location.path('schedules');
      }, function(errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

  }]);
