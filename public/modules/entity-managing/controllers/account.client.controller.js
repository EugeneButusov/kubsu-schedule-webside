/**
 * Created by eugene on 01.12.14.
 */

'use strict';

angular.module('entity-managing').controller('AccountController', ['$scope', '$stateParams', '$location', 'Authentication', 'Accounts',
    function ($scope, $stateParams, $location, Authentication, Accounts) {
        $scope.authentication = Authentication;

        $scope.find = function() {
            $scope.users = Accounts.query();
        };

        $scope.create = function() {
        };

        $scope.findOne = function() {
            $scope.kinds = [
                {
                    name: 'Студент',
                    value: 'student'
                },
                {
                    name: 'Преподаватель',
                    value: 'teacher'
                }
            ];
            $scope.availableRoles = [
                {
                    name: 'Читатель',
                    value: 'viewer',
                    selected: false
                },
                {
                    name: 'Редактор',
                    value: 'editor',
                    selected: false
                },
                {
                    name: 'Администратор',
                    value: 'admin',
                    selected: false
                }
            ];
            $scope.selectedRoles = [];
            Accounts.get({
                accountId: $stateParams.accountId
            }, function(account) {
                $scope.account = account;
                for (var i = 0; i < $scope.availableRoles.length; i++) {
                    if ($scope.availableRoles[i].value == account.roles) {
                        $scope.availableRoles[i].selected = true;
                    }
                }
            });
        };

        $scope.remove = function (user) {
            if (user) {
                user.$remove();

                for (var i in $scope.users) {
                    if ($scope.users[i] === user) {
                        $scope.users.splice(i, 1);
                    }
                }
            } else {
                $scope.user.$remove(function() {
                    $location.path('accounts');
                });
            }
        };

        $scope.update = function() {
            var account = $scope.account;
            var newRoles = [];
            for (var i = 0; i < $scope.selectedRoles.length; i++) {
                newRoles.push($scope.selectedRoles[i].value);
            }
            account.roles = newRoles;

            account.$update(function() {
                $location.path('accounts');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

    }]);
