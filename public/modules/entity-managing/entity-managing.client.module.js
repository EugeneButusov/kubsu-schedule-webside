/**
 * Created by eugene on 14.11.14.
 */

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('entity-managing', ['ui.tree', 'daterangepicker']);
