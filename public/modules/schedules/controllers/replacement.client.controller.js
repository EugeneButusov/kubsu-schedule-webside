/**
 * Created by eugene on 20.05.15.
 */

'use strict';

angular.module('entity-managing')
  .controller('ReplacementController',
  ['$scope', '$stateParams', '$location', 'Authentication', 'Replacements', 'Subjects', 'LessonTypes', 'Lessons',
    'Accounts', 'Unions', 'Playgrounds',
    function ($scope, $stateParams, $location, Authentication, Replacements, Subjects, LessonTypes, Lessons, Accounts,
              Unions, Playgrounds) {
      $scope.authentication = Authentication;

      $scope.prepare = function() {
        $scope.subjects = Subjects.query();
        $scope.lessonTypes = LessonTypes.query();
        $scope.playgrounds = Playgrounds.query();
        $scope.lessons = Lessons.query();
        Accounts.query({kind: 'teacher'}, function(users) {
          $scope.teachers = users;
        });
        $scope.learningUnions = Unions.query(null, function(unions) {
          $scope.learningUnions = [];
          for (var i = 0; i < unions.length; i++) {
            var union = unions[i];
            if (!union.parent) {
              union.children = buildSubtree(union, unions);
              $scope.learningUnions.push(union);
            }
          }
        });
      };

      function buildSubtree(node, unions) {
        var result = [];
        for (var i = 0; i < unions.length; i++) {
          if (node._id == unions[i].parent) {
            result.push(unions[i]);
          }
        }
        for (i = 0; i < result.length; i++) {
          result[i].children = buildSubtree(result[i], unions);
        }
        return result;
      }

      $scope.find = function() {
        $scope.replacements = Replacements.query();
      };


      $scope.create = function() {
        function populateUnions(myUnions, rootUnion) {
          if (rootUnion.selected) {
            myUnions.push(rootUnion._id);
          }
          for (var i in rootUnion.children) {
            populateUnions(myUnions, rootUnion.children[i]);
          }
        }
        var myLearningUnions = [];
        populateUnions(myLearningUnions, this.learningUnions[0]);
        var replacement = new Replacements({
          at: new Date(this.at.split('-')[0], this.at.split('-')[1] - 1, this.at.split('-')[2]),
          subject: this.subject._id,
          type: this.type,
          lesson: this.lesson,
          teacher: this.teacher,
          playground: this.playground,
          learningUnions: myLearningUnions
        });
        replacement.$save(function() {
          $location.path('replacements');

        }, function(errorResponse) {
          $scope.error = errorResponse.data.message;
        });
      };

      $scope.findOne = function() {
        $scope.subjects = Subjects.query();
        $scope.lessonTypes = LessonTypes.query();
        $scope.playgrounds = Playgrounds.query();
        $scope.lessons = Lessons.query();
        $scope.replacement = Replacements.get({
          replacementId: $stateParams.replacementId
        });
        Accounts.query({kind: 'teacher'}, function(users) {
          $scope.teachers = users;
        });
        $scope.learningUnions = Unions.query(null, function(unions) {
          $scope.learningUnions = [];
          for (var i = 0; i < unions.length; i++) {
            var union = unions[i];
            if ($scope.replacement.learningUnions.indexOf(union._id) > -1) {
              union.selected = true;
            }
            if (!union.parent) {
              union.children = buildSubtree(union, unions);
              $scope.learningUnions.push(union);
            }
          }
        });
      };

      $scope.remove = function (replacement) {
        if (replacement) {
          replacement.$remove();

          for (var i in $scope.replacements) {
            if ($scope.replacements[i] === replacements) {
              $scope.replacement.splice(i, 1);
            }
          }
        } else {
          $scope.replacement.$remove(function() {
            $location.path('replacements');
          });
        }
      };


      $scope.updateUnionSelection = function(union) {
        union.selected = !union.selected;
        for (var i in union.children) {
          $scope.updateUnionSelection(union.children[i]);
        }
      };

      $scope.update = function() {
        var replacement = $scope.replacement;

        replacement.$update(function() {
          $location.path('replacements');
        }, function(errorResponse) {
          $scope.error = errorResponse.data.message;
        });
      };

    }]);
