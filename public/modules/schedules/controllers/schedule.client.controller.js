/**
 * Created by eugene on 10.05.15.
 */

//'use strict';

angular.module('schedules').controller('SchedulingController', ['$scope', 'lodash', '$stateParams', '$location', 'Authentication', 'Schedules', 'Unions', 'Lessons',
  function ($scope, lodash, $stateParams, $location, Authentication, Schedules, Unions, Lessons) {
    $scope.authentication = Authentication;

    $scope.prepare = function() {
      $scope.unions = Unions.query();
      $scope.lessons = Lessons.query({}, function(lessons) {
        $scope.lessons = lessons.sort(function(a, b) {
          return a.number > b.number;
        });
      });
    };
    $scope.fetch = function() {
      Schedules.query({
        startDate: $scope.from + 'T00:00:00.000',
        endDate: $scope.to + 'T00:00:00.000',
        union: $scope.union
      }, function(schedules) {
        for (var i in schedules) {
          schedules[i].learningUnionNames = lodash.pluck(schedules[i].learningUnions, 'name');
        }
        var dates = lodash.map(lodash.uniq(lodash.pluck(schedules, 'at')), function(val) {
          return new Date(val);
        }).sort(function(a, b) {
          return (a.getDate() > b.getDate());
        });
        var result = [];
        lodash.forEach(dates, function(date) {
          var record = {
            date: date
          };
          record.items = [];
          lodash.forEach($scope.lessons, function(lesson) {
            var tmp = {};
            lodash.forEach(schedules, function(s) {
              if (new Date(s.at).getDate() == date.getDate() && s.lesson._id == lesson._id) {
                tmp = s;
              }
            });
            record.items[record.items.length] = tmp;
          });
          result[result.length] = record;
        });
        $scope.schedules = result;
      });
    };
  }]);
