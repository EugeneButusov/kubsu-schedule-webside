/**
 * Created by eugene on 10.05.15.
 */

'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('schedules', ['ui.tree', 'daterangepicker', 'ngLodash']);
