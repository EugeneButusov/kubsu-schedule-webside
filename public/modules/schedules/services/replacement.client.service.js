/**
 * Created by eugene on 20.05.15.
 */


'use strict';

angular.module('entity-managing').factory('Replacements', ['$resource',
  function($resource) {
    return $resource('replacements/:replacementId', {
      replacementId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);
