/**
 * Created by eugene on 10.05.15.
 */

'use strict';

angular.module('entity-managing').run(['Menus',
  function(Menus) {
    // Set top bar menu items
    Menus.addMenuItem('topbar', 'Расписание', 'scheduling');
  }
]);
