/**
 * Created by eugene on 10.05.15.
 */

'use strict';

// Setting up route
angular.module('schedules').config(['$stateProvider',
  function($stateProvider) {
    // Lessons state routing
    $stateProvider.
      state('scheduling', {
        url: '/scheduling',
        templateUrl: 'modules/schedules/views/main.client.view.html'
      }).
      state('replacements', {
        url: '/replacements',
        templateUrl: 'modules/schedules/views/replacements/list.client.view.html'
      }).
      state('createReplacement', {
        url: '/replacements/create',
        templateUrl: 'modules/schedules/views/replacements/new.client.view.html'
      }).
      state('editReplacement', {
        url: '/replacements/:replacementId/edit',
        templateUrl: 'modules/schedules/views/replacements/edit.client.view.html'
      });
  }
]);
