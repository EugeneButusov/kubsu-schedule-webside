'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'mean';
	var applicationModuleVendorDependencies = ['ngResource', 'ngAnimate', 'ui.router', 'ui.bootstrap', 'ui.utils'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();
'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
/**
 * Created by eugene on 14.11.14.
 */

'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('entity-managing', ['ui.tree']);
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');
'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		});
	}
]);
'use strict';

angular.module('core').controller('HeaderController', ['$scope', 'Authentication', 'Menus',
	function($scope, Authentication, Menus) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.menu = Menus.getMenu('topbar');

		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});
	}
]);
'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication',
	function($scope, Authentication) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
	}
]);
'use strict';

//Menu service used for managing  menus
angular.module('core').service('Menus', [

	function() {
		// Define a set of default roles
		this.defaultRoles = ['*'];

		// Define the menus object
		this.menus = {};

		// A private function for rendering decision 
		var shouldRender = function(user) {
			if (user) {
				if (!!~this.roles.indexOf('*')) {
					return true;
				} else {
					for (var userRoleIndex in user.roles) {
						for (var roleIndex in this.roles) {
							if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
								return true;
							}
						}
					}
				}
			} else {
				return this.isPublic;
			}

			return false;
		};

		// Validate menu existance
		this.validateMenuExistance = function(menuId) {
			if (menuId && menuId.length) {
				if (this.menus[menuId]) {
					return true;
				} else {
					throw new Error('Menu does not exists');
				}
			} else {
				throw new Error('MenuId was not provided');
			}

			return false;
		};

		// Get the menu object by menu id
		this.getMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			return this.menus[menuId];
		};

		// Add new menu object by menu id
		this.addMenu = function(menuId, isPublic, roles) {
			// Create the new menu
			this.menus[menuId] = {
				isPublic: isPublic || false,
				roles: roles || this.defaultRoles,
				items: [],
				shouldRender: shouldRender
			};

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			delete this.menus[menuId];
		};

		// Add menu item object
		this.addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Push new menu item
			this.menus[menuId].items.push({
				title: menuItemTitle,
				link: menuItemURL,
				menuItemType: menuItemType || 'item',
				menuItemClass: menuItemType,
				uiRoute: menuItemUIRoute || ('/' + menuItemURL),
				isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].isPublic : isPublic),
				roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].roles : roles),
				position: position || 0,
				items: [],
				shouldRender: shouldRender
			});

			// Return the menu object
			return this.menus[menuId];
		};

		// Add submenu item object
		this.addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
					// Push new submenu item
					this.menus[menuId].items[itemIndex].items.push({
						title: menuItemTitle,
						link: menuItemURL,
						uiRoute: menuItemUIRoute || ('/' + menuItemURL),
						isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].items[itemIndex].isPublic : isPublic),
						roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : roles),
						position: position || 0,
						shouldRender: shouldRender
					});
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenuItem = function(menuId, menuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
					this.menus[menuId].items.splice(itemIndex, 1);
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeSubMenuItem = function(menuId, submenuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
					if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
						this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
					}
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		//Adding the topbar menu
		this.addMenu('topbar');
	}
]);
/**
 * Created by eugene on 14.11.14.
 */

'use strict';

angular.module('entity-managing').run(['Menus',
    function(Menus) {
        // Set top bar menu items
        Menus.addMenuItem('topbar', 'Управление', 'entity-managing', 'dropdown');
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Пользователи', 'users');
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Объединения', 'unions');
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Занятия', 'lessons');
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Площадки', 'playgrounds');
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Типы занятий', 'lessonTypes');
        Menus.addSubMenuItem('topbar', 'entity-managing', 'Предметы', 'subjects');
    }
]);
/**
 * Created by eugene on 14.11.14.
 */

'use strict';

// Setting up route
angular.module('entity-managing').config(['$stateProvider',
    function($stateProvider) {
        // Lessons state routing
        $stateProvider.
            state('listLessons', {
                url: '/lessons',
                templateUrl: 'modules/entity-managing/views/lessons/list.client.view.html'
            }).
            state('createLesson', {
                url: '/lessons/create',
                templateUrl: 'modules/entity-managing/views/lessons/new.client.view.html'
            }).
            state('editLesson', {
                url: '/lessons/:lessonId/edit',
                templateUrl: 'modules/entity-managing/views/lessons/edit.client.view.html'
            });

        //Playgrounds state routing
        $stateProvider.
            state('listPlaygrounds', {
                url: '/playgrounds',
                templateUrl: 'modules/entity-managing/views/playgrounds/list.client.view.html'
            }).
            state('createPlayground', {
                url: '/playgrounds/create',
                templateUrl: 'modules/entity-managing/views/playgrounds/new.client.view.html'
            }).
            state('editPlayground', {
                url: '/playgrounds/:playgroundId/edit',
                templateUrl: 'modules/entity-managing/views/playgrounds/edit.client.view.html'
            });

        //Unions state routing
        $stateProvider.
            state('listUnions', {
                url: '/unions',
                templateUrl: 'modules/entity-managing/views/unions/treelist.client.view.html'
            });

        //Subjects state config
        $stateProvider.
            state('listSubjects', {
                url: '/subjects',
                templateUrl: 'modules/entity-managing/views/subjects/list.client.view.html'
            }).
            state('createSubject', {
                url: '/subjects/create',
                templateUrl: 'modules/entity-managing/views/subjects/new.client.view.html'
            }).
            state('editSubject', {
                url: '/subjects/:subjectId/edit',
                templateUrl: 'modules/entity-managing/views/subjects/edit.client.view.html'
            });

        //Lesson types state config
        $stateProvider.
            state('listLessonTypes', {
                url: '/lesson-types',
                templateUrl: 'modules/entity-managing/views/lesson-types/list.client.view.html'
            }).
            state('createLessonType', {
                url: '/lesson-types/create',
                templateUrl: 'modules/entity-managing/views/lesson-types/new.client.view.html'
            }).
            state('editLessonType', {
                url: '/lesson-types/:lessonTypeId/edit',
                templateUrl: 'modules/entity-managing/views/lesson-types/edit.client.view.html'
            });

        $stateProvider.
            state('listAccounts', {
                url: '/users',
                templateUrl: 'modules/entity-managing/views/accounts/list.client.view.html'
            });
        $stateProvider.
          state('createSchedule', {
            url: '/schedules/create',
            templateUrl: 'modules/entity-managing/views/schedules/new.client.view.html'
          });
    }
]);

/**
 * Created by eugene on 01.12.14.
 */

'use strict';

angular.module('entity-managing').controller('AccountController', ['$scope', '$stateParams', '$location', 'Authentication', 'Users',
    function ($scope, $stateParams, $location, Authentication, Users) {
        $scope.authentication = Authentication;

        $scope.find = function() {
            $scope.users = Users.query();
        };

        $scope.create = function() {
        };

        $scope.findOne = function() {
        };

        $scope.remove = function (user) {
        };

        $scope.update = function() {
        };

    }]);
/**
 * Created by eugene on 14.11.14.
 */

'use strict';

angular.module('entity-managing').controller('LessonController', ['$scope', '$stateParams', '$location', 'Authentication', 'Lessons',
    function ($scope, $stateParams, $location, Authentication, Lessons) {
        $scope.authentication = Authentication;

        $scope.find = function() {
            $scope.lessons = Lessons.query();
        };

        $scope.create = function() {
            var lesson = new Lessons({
                number: this.number,
                beginTime: new Date(1970, 0, 1, this.beginTime.split(':')[0], this.beginTime.split(':')[1]),
                endTime: new Date(1970, 0, 1, this.endTime.split(':')[0], this.endTime.split(':')[1])
            });
            lesson.$save(function() {
                $location.path('lessons');

            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.findOne = function() {
            $scope.lesson = Lessons.get({
                lessonId: $stateParams.lessonId
            });
        };

        $scope.remove = function (lesson) {
            if (lesson) {
                lesson.$remove();

                for (var i in $scope.lessons) {
                    if ($scope.lessons[i] === lesson) {
                        $scope.lessons.splice(i, 1);
                    }
                }
            } else {
                $scope.lesson.$remove(function() {
                    $location.path('lessons');
                });
            }
        };

        $scope.update = function() {
            var lesson = $scope.lesson;
            lesson.beginTime =  new Date(1970, 0, 1, this.beginTime.split(':')[0], this.beginTime.split(':')[1]);
            lesson.endTime = new Date(1970, 0, 1, this.endTime.split(':')[0], this.endTime.split(':')[1]);

            lesson.$update(function() {
                $location.path('lessons');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

    }]);
/**
 * Created by eugene on 28.11.14.
 */

'use strict';

angular.module('entity-managing').controller('LessonTypeController', ['$scope', '$stateParams', '$location', 'Authentication', 'LessonTypes',
    function ($scope, $stateParams, $location, Authentication, LessonTypes) {
        $scope.authentication = Authentication;

        $scope.find = function() {
            $scope.lessonTypes = LessonTypes.query();
        };

        $scope.create = function() {
            var lessonType = new LessonTypes({
                title: this.title
            });
            lessonType.$save(function() {
                $location.path('lessonTypes');

            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.findOne = function() {
            $scope.lessonType = LessonTypes.get({
                lessonTypeId: $stateParams.lessonTypeId
            });
        };

        $scope.remove = function (lessonType) {
            if (lessonType) {
                lessonType.$remove();

                for (var i in $scope.lessonTypes) {
                    if ($scope.lessonTypes[i] === lessonType) {
                        $scope.lessonTypes.splice(i, 1);
                    }
                }
            } else {
                $scope.lessonType.$remove(function() {
                    $location.path('lessonTypes');
                });
            }
        };

        $scope.update = function() {
            var lessonType = $scope.lessonType;

            lessonType.$update(function() {
                $location.path('lessonTypes');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

    }]);
/**
 * Created by eugene on 15.11.14.
 */

'use strict';

angular.module('entity-managing').controller('PlaygroundController', ['$scope', '$stateParams', '$location', 'Authentication', 'Playgrounds',
    function ($scope, $stateParams, $location, Authentication, Playgrounds) {
        $scope.authentication = Authentication;

        $scope.find = function() {
            $scope.playgrounds = Playgrounds.query();
        };

        $scope.create = function() {
            var playground = new Playgrounds({
                name: this.name
            });
            playground.$save(function() {
                $location.path('playgrounds');

            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.findOne = function() {
            $scope.playground = Playgrounds.get({
                playgroundId: $stateParams.playgroundId
            });
        };

        $scope.remove = function (playground) {
            if (playground) {
                playground.$remove();

                for (var i in $scope.playgrounds) {
                    if ($scope.playgrounds[i] === playground) {
                        $scope.playgrounds.splice(i, 1);
                    }
                }
            } else {
                $scope.playground.$remove(function() {
                    $location.path('playgrounds');
                });
            }
        };

        $scope.update = function() {
            var playground = $scope.playground;

            playground.$update(function() {
                $location.path('playgrounds');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

    }]);
/**
 * Created by eugene on 10.03.15.
 */

/**
 * Created by eugene on 24.11.14.
 */

'use strict';

angular.module('entity-managing').controller('ScheduleController', ['$scope', '$stateParams', '$location', 'Authentication', 'Schedules', 'Subjects',
  function ($scope, $stateParams, $location, Authentication, Schedules, Subjects) {
    $scope.authentication = Authentication;

    $scope.prepare = function() {
      $scope.subjects = Subjects.query();
    };

    $scope.find = function() {
      $scope.schedules = Schedules.query();
    };

    $scope.create = function() {
      var schedule = new Schedules({
        /*title: this.title,
        description: this.description*/
      });
      schedule.$save(function() {
        $location.path('schedules');

      }, function(errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

    $scope.findOne = function() {
      $scope.schedule = Schedules.get({
        scheduleId: $stateParams.scheduleId
      });
    };

    $scope.remove = function (schedule) {
      if (schedule) {
        schedule.$remove();

        for (var i in $scope.schedules) {
          if ($scope.schedules[i] === schedules) {
            $scope.schedule.splice(i, 1);
          }
        }
      } else {
        $scope.schedule.$remove(function() {
          $location.path('schedules');
        });
      }
    };

    $scope.update = function() {
      var schedule = $scope.schedule;

      schedule.$update(function() {
        $location.path('schedules');
      }, function(errorResponse) {
        $scope.error = errorResponse.data.message;
      });
    };

  }]);

/**
 * Created by eugene on 24.11.14.
 */

'use strict';

angular.module('entity-managing').controller('SubjectController', ['$scope', '$stateParams', '$location', 'Authentication', 'Subjects',
    function ($scope, $stateParams, $location, Authentication, Subjects) {
        $scope.authentication = Authentication;

        $scope.find = function() {
            $scope.subjects = Subjects.query();
        };

        $scope.create = function() {
            var subject = new Subjects({
                title: this.title,
                description: this.description
            });
            subject.$save(function() {
                $location.path('subjects');

            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.findOne = function() {
            $scope.subject = Subjects.get({
                subjectId: $stateParams.subjectId
            });
        };

        $scope.remove = function (subject) {
            if (subject) {
                subject.$remove();

                for (var i in $scope.subjects) {
                    if ($scope.subjects[i] === subject) {
                        $scope.subjects.splice(i, 1);
                    }
                }
            } else {
                $scope.subject.$remove(function() {
                    $location.path('subjects');
                });
            }
        };

        $scope.update = function() {
            var subject = $scope.subject;

            subject.$update(function() {
                $location.path('subjects');
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

    }]);
/**
 * Created by eugene on 23.11.14.
 */

'use strict';

angular.module('entity-managing').controller('UnionController', ['$scope', '$stateParams', '$location', 'Authentication', 'Unions',
    function ($scope, $stateParams, $location, Authentication, Unions) {
        $scope.authentication = Authentication;

        $scope.find = function() {
            $scope.unions = Unions.query(null, function(unions) {
                $scope.unions = [];
                for (var i = 0; i < unions.length; i++) {
                    var union = unions[i];
                    if (!union.parent) {
                        union.children = buildSubtree(union, unions);
                        $scope.unions.push(union);
                    }
                }
            });
        };

        function buildSubtree(node, unions) {
            var result = [];
            for (var i = 0; i < unions.length; i++) {
                if (node._id == unions[i].parent) {
                    result.push(unions[i]);
                }
            }
            for (i = 0; i < result.length; i++) {
                result[i].children = buildSubtree(result[i], unions);
            }
            return result;
        }

        $scope.create = function(parent) {
            var union = new Unions({
                name: 'Новое объединение: ' + (new Date()).toString(),
                parent:parent._id
            });
            union.$save(function(union) {
                if (!parent.children) {
                    parent.children = [];
                }
                parent.children.push(union);
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };

        $scope.findOne = function() {
            $scope.union = Unions.get({
                unionId: $stateParams.unionId
            });
        };

        function findParentNode(node, currentNode) {
            if (node.parent == currentNode._id) {
                return currentNode;
            }
            else {
                for (var i = 0; i < currentNode.children.length; i++) {
                    var temp = findParentNode(node, currentNode.children[i]);
                    if (temp) {
                        return temp;
                    }
                }
                return null;
            }
        }

        $scope.drop = function (union) {
            union.$remove();
            var parentNode = null;
            for (var i = 0; i < $scope.unions.length; i++) {
                if ((parentNode = findParentNode(union, $scope.unions[i]))) {
                    break;
                }
            }

            for (i in parentNode.children) {
                if (parentNode.children[i] === union) {
                    parentNode.children.splice(i, 1);
                }
            }
        };

        $scope.beginUpdate = function(union) {
            union.editing = true;
        };
        $scope.cancelEditing = function(union) {
            union.editing = false;
        };

        $scope.save = function(union) {
            union.$update(function(union) {
                union.editing = false;
            }, function(errorResponse) {
                $scope.error = errorResponse.data.message;
            });
        };
    }]);
/**
 * Created by eugene on 01.12.14.
 */

'use strict';

angular.module('entity-managing').factory('Users', ['$resource',
    function($resource) {
        return $resource('users/:userId', {
            userId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
/**
 * Created by eugene on 28.11.14.
 */

'use strict';

//Lesson types service used for communicating with the articles REST endpoints
angular.module('entity-managing').factory('LessonTypes', ['$resource',
    function($resource) {
        return $resource('lesson-types/:lessonTypeId', {
            lessonTypeId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
/**
 * Created by eugene on 15.11.14.
 */

'use strict';

//Lessons service used for communicating with the articles REST endpoints
angular.module('entity-managing').factory('Lessons', ['$resource',
    function($resource) {
        return $resource('lessons/:lessonId', {
            lessonId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
/**
 * Created by eugene on 15.11.14.
 */
'use strict';

//Playgrounds service used for communicating with the articles REST endpoints
angular.module('entity-managing').factory('Playgrounds', ['$resource',
    function($resource) {
        return $resource('playgrounds/:playgroundId', {
            playgroundId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
/**
 * Created by eugene on 10.03.15.
 */

'use strict';

angular.module('entity-managing').factory('Schedules', ['$resource',
  function($resource) {
    return $resource('schedules/:scheduleId', {
      scheduleId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });
  }
]);

/**
 * Created by eugene on 24.11.14.
 */

'use strict';

//Lessons service used for communicating with the articles REST endpoints
angular.module('entity-managing').factory('Subjects', ['$resource',
    function($resource) {
        return $resource('subjects/:subjectId', {
            subjectId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
/**
 * Created by eugene on 23.11.14.
 */

'use strict';

//Unions service used for communicating with the articles REST endpoints
angular.module('entity-managing').factory('Unions', ['$resource',
    function($resource) {
        return $resource('unions/:unionId', {
            unionId: '@_id'
        }, {
            update: {
                method: 'PUT'
            }
        });
    }
]);
'use strict';

// Config HTTP Error Handling
angular.module('users').config(['$httpProvider',
	function($httpProvider) {
		// Set the httpProvider "not authorized" interceptor
		$httpProvider.interceptors.push(['$q', '$location', 'Authentication',
			function($q, $location, Authentication) {
				return {
					responseError: function(rejection) {
						switch (rejection.status) {
							case 401:
								// Deauthenticate the global user
								Authentication.user = null;

								// Redirect to signin page
								$location.path('signin');
								break;
							case 403:
								// Add unauthorized behaviour 
								break;
						}

						return $q.reject(rejection);
					}
				};
			}
		]);
	}
]);
'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('profile', {
			url: '/settings/profile',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.view.html'
		}).
		state('accounts', {
			url: '/settings/accounts',
			templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
		}).
		state('signup', {
			url: '/signup',
			templateUrl: 'modules/users/views/authentication/signup.client.view.html'
		}).
		state('signin', {
			url: '/signin',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html'
		}).
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
		}).
		state('reset-invalid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.view.html'
		});
	}
]);
'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication',
	function($scope, $http, $location, Authentication) {
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		$scope.signup = function() {
			$http.post('/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		$scope.signin = function() {
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
'use strict';

angular.module('users').controller('PasswordController', ['$scope', '$stateParams', '$http', '$location', 'Authentication',
	function($scope, $stateParams, $http, $location, Authentication) {
		$scope.authentication = Authentication;

		//If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		// Submit forgotten password account id
		$scope.askForPasswordReset = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/forgot', $scope.credentials).success(function(response) {
				// Show user success message and clear form
				$scope.credentials = null;
				$scope.success = response.message;

			}).error(function(response) {
				// Show user error message and clear form
				$scope.credentials = null;
				$scope.error = response.message;
			});
		};

		// Change user password
		$scope.resetUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;

				// Attach user profile
				Authentication.user = response;

				// And redirect to the index page
				$location.path('/password/reset/success');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication', 'Unions',
	function($scope, $http, $location, Users, Authentication, Unions) {
		$scope.user = Authentication.user;

		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');

		// Check if there are additional accounts 
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.user = Authentication.user = response;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid) {
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);

				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
				}, function(response) {
					$scope.error = response.data.message;
				});
			} else {
				$scope.submitted = true;
			}
		};

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

    $scope.preload = function() {
      $scope.unions = Unions.query(null, function(unions) {
        $scope.unions = [];
        for (var i = 0; i < unions.length; i++) {
          var union = unions[i];
          if (!union.parent) {
            union.children = buildSubtree(union, unions);
            $scope.unions.push(union);
          }
        }
      });

      function buildSubtree(node, unions) {
        var result = [];
        for (var i = 0; i < unions.length; i++) {
          if (node._id == unions[i].parent) {
            result.push(unions[i]);
          }
        }
        for (i = 0; i < result.length; i++) {
          result[i].children = buildSubtree(result[i], unions);
        }
        return result;
      }
    };
	}
]);

'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', [
	function() {
		var _this = this;

		_this._data = {
			user: window.user
		};

		return _this._data;
	}
]);
'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('users', {}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
