/**
 * Created by eugene on 23.11.14.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('../errors.server.controller'),
    Union = mongoose.model('Union'),
    _ = require('lodash');

/**
 * Create an union
 */
exports.create = function(req, res) {
    var union = new Union(req.body);

    union.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(union);
        }
    });
};

/**
 * Show the current union
 */
exports.read = function(req, res) {
    res.json(req.union);
};

/**
 * Update a union
 */
exports.update = function(req, res) {
    var union = req.union;

    union = _.extend(union, req.body);

    union.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(union);
        }
    });
};

/**
 * Delete an union
 */
exports.delete = function(req, res) {
    var union = req.union;

    union.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(union);
        }
    });
};

/**
 * List of Unions
 */
exports.list = function(req, res) {
    Union.find().exec(function(err, unions) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(unions);
        }
    });
};

/**
 * Union middleware
 */
exports.unionByID = function(req, res, next, id) {
    Union.findById(id).exec(function(err, union) {
        if (err) return next(err);
        if (!union) return next(new Error('Failed to load union ' + id));
        req.union = union;
        next();
    });
};

/**
 * Union authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    /*if (req.article.user.id !== req.user.id) {
     return res.status(403).send({
     message: 'User is not authorized'
     });
     }*/
    next();
};