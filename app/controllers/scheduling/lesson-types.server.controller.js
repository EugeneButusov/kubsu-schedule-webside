/**
 * Created by eugene on 28.11.14.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('../errors.server.controller'),
    LessonType = mongoose.model('LessonType'),
    _ = require('lodash');

/**
 * Create a lesson type
 */
exports.create = function(req, res) {
    var lessonType = new LessonType(req.body);

    lessonType.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(lessonType);
        }
    });
};

/**
 * Show the current lesson type
 */
exports.read = function(req, res) {
    res.json(req.lessonType);
};

/**
 * Update a lesson type
 */
exports.update = function(req, res) {
    var lessonType = req.lessonType;

    lessonType = _.extend(lessonType, req.body);

    lessonType.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(lessonType);
        }
    });
};

/**
 * Delete an lesson type
 */
exports.delete = function(req, res) {
    var lessonType = req.lessonType;

    lessonType.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(lessonType);
        }
    });
};

/**
 * List of Lesson types
 */
exports.list = function(req, res) {
    LessonType.find().exec(function(err, lessonTypes) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(lessonTypes);
        }
    });
};

/**
 * Lesson type middleware
 */
exports.lessonTypeByID = function(req, res, next, id) {
    LessonType.findById(id).exec(function(err, lessonType) {
        if (err) return next(err);
        if (!lessonType) return next(new Error('Failed to load lesson type ' + id));
        req.lessonType = lessonType;
        next();
    });
};

/**
 * Lesson type authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    if (req.user) {
        return res.status(403).send({
            message: 'User is not authorized'
        });
    }
    next();
};
