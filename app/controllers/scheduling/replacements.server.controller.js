/**
 * Created by eugene on 20.05.15.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('../errors.server.controller'),
  Replacement = mongoose.model('Replacement'),
  _ = require('lodash'),
  Q = require('q');

/**
 * Create a replacement
 */
exports.create = function(req, res) {
  req.body.author = req.user._id;
  Q
    .ninvoke(new Replacement(req.body), 'save')
    .spread(function(replacement) {
      res.json(replacement);
    })
    .catch(function(error) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(error)
      });
    });
};

/**
 * Show the current replacement
 */
exports.read = function(req, res) {
  res.json(req.replacement);
};

/**
 * Update a replacement
 */
exports.update = function(req, res) {
  var replacement = req.replacement;

  replacement = _.extend(replacement, req.body);

  replacement.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(replacement);
    }
  });
};

/**
 * Delete an replacement
 */
exports.delete = function(req, res) {
  var replacement = req.replacement;

  replacement.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(replacement);
    }
  });
};

/**
 * List of replacements
 */

exports.list = function(req, res) {
  var queryParams = {};
  if (req.user.roles.indexOf('editor') != -1) {
    queryParams.author = req.user._id;
  }
  Q
    .ninvoke(Replacement.find(queryParams)
      .populate('author subject type lesson teacher learningUnions playground'), 'exec')
    .then(function(replacements) {
      res.json(replacements);
    })
    .catch(function(error) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(error.message)
      });
    });
};

/**
 * replacement middleware
 */
exports.replacementByID = function(req, res, next, id) {
  Replacement.findById(id).exec(function(err, replacement) {
    if (err) return next(err);
    if (!replacement) return next(new Error('Failed to load replacement ' + id));
    req.replacement = replacement;
    next();
  });
};

/*** replacement authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
  if (req.user.roles.indexOf('editor') != -1 || req.user.roles.indexOf('admin') != -1) {
    return next(new Error('Forbidden'));
  }
  next();
};
