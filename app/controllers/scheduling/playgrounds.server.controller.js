/**
 * Created by eugene on 15.11.14.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('../errors.server.controller'),
    Playground = mongoose.model('Playground'),
    _ = require('lodash');

/**
 * Create a playground
 */
exports.create = function(req, res) {
    var playground = new Playground(req.body);

    playground.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(playground);
        }
    });
};

/**
 * Show the current playground
 */
exports.read = function(req, res) {
    res.json(req.playground);
};

/**
 * Update a playground
 */
exports.update = function(req, res) {
    var playground = req.playground;

    playground = _.extend(playground, req.body);

    playground.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(playground);
        }
    });
};

/**
 * Delete an playground
 */
exports.delete = function(req, res) {
    var playground = req.playground;

    playground.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(playground);
        }
    });
};

/**
 * List of Lessons
 */
exports.list = function(req, res) {
    Playground.find().exec(function(err, playgrounds) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(playgrounds);
        }
    });
};

/**
 * Playground middleware
 */
exports.playgroundByID = function(req, res, next, id) {
    Playground.findById(id).exec(function(err, playground) {
        if (err) return next(err);
        if (!playground) return next(new Error('Failed to load playground ' + id));
        req.playground = playground;
        next();
    });
};

/**
 * Playground authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    /*if (req.article.user.id !== req.user.id) {
     return res.status(403).send({
     message: 'User is not authorized'
     });
     }*/
    next();
};