/**
 * Created by eugene on 10.03.15.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('../errors.server.controller'),
  Schedule = mongoose.model('Schedule'),
  Replacement = mongoose.model('Replacement'),
  AppConfiguration = mongoose.model('AppConfiguration'),
  _ = require('lodash'),
  Q = require('q');

/**
 * Create a schedule
 */
exports.create = function(req, res) {
  Q
    .ninvoke(new Schedule(req.body), 'save')
    .spread(function(schedule) {
      res.json(schedule);
    })
    .catch(function(error) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
};

/**
 * Show the current schedule
 */
exports.read = function(req, res) {
  res.json(req.schedule);
};

/**
 * Update a schedule
 */
exports.update = function(req, res) {
  var schedule = req.schedule;

  schedule = _.extend(schedule, req.body);

  schedule.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(schedule);
    }
  });
};

/**
 * Delete an schedule
 */
exports.delete = function(req, res) {
  var schedule = req.schedule;

  schedule.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(schedule);
    }
  });
};

/**
 * List of schedules
 */

exports.list = function(req, res) {
  if (req.query.startDate) {
    var _startDate = new Date(req.query.startDate), _endDate = new Date(req.query.endDate);

    var _result = [];
    var _numberOfDaysInCycle, _freeDays, _cycleStartDate;
    var _union;
    if (req.query.union) {
      _union = req.query.union;
    }
    Q
      .ninvoke(AppConfiguration.findOne({active: true}), 'exec')
      .then(function(activeConfiguration) {
        _numberOfDaysInCycle = activeConfiguration.scheduleCycle.length;
        _freeDays = activeConfiguration.scheduleCycle.freeDays;
        _cycleStartDate = activeConfiguration.scheduleCycle.startDate;
      })
      .then(function() {
        var promises = [];
        for (var i = new Date(_startDate); i <= _endDate; i.setDate(i.getDate() + 1)) {
          var _i = i;
          promises.push((function (date) {
            var dayPosition = Math.round((date.getTime() - _cycleStartDate.getTime()) / (60 * 60 * 24 * 1000));
            dayPosition %= _numberOfDaysInCycle;
            dayPosition++;
            var _date = new Date(date);
            var filter = {};
            filter.cyclePosition = dayPosition;
            if (_union != undefined) {
              filter.learningUnions = _union;
            }
            return Q.ninvoke(Schedule.find(filter).populate('subject type lesson teacher learningUnions playground'), 'exec')
              .then(function(schedules) {
                for (var j in schedules) {
                  var schedule = schedules[j];
                  delete schedule._doc.cyclePosition;
                  schedule._doc.at = _date;
                  _result.push(schedule);

                }
              });
          })(_i));
        }
        return Q.all(promises);
      })
      .then(function() {
        var filter = {
          at: {
            $gte: _startDate,
            $lte: _endDate
          }
        };
        return Q
          .ninvoke(Replacement.find(filter).populate('subject type lesson teacher learningUnions playground'), 'exec')
          .then(function(replacements) {
            _.forEach(replacements, function(replacement) {
              delete replacement._doc.author;
              _result.push(replacement);
            });
            for (var i = 0; i < _result.length; i++) {
              for (var j = 0; j < _result.length; j++) {
                if (_result[i]._doc.at === _result[j]._doc.at
                  && _result[i]._doc.lesson === _result[j]._doc.lesson
                  && i !== j) {
                  _result.splice(j, 1);
                  j--;
                }
              }
            }
            res.json(_result);
          });
      })
      .catch(function(error) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(error.message)
        });
      });
  } else {
    Q
      .ninvoke(Schedule.find().populate('subject type lesson teacher learningUnions playground'), 'exec')
      .then(function(schedules) {
        res.json(schedules);
      })
      .catch(function(error) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(error.message)
        });
      });
  }
};

/**
 * schedule middleware
 */
exports.scheduleByID = function(req, res, next, id) {
  Schedule.findById(id).populate('subject').exec(function(err, schedule) {
    if (err) return next(err);
    if (!schedule) return next(new Error('Failed to load schedule ' + id));
    req.schedule = schedule;
    next();
  });
};

/*** schedule authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
  next();
};
