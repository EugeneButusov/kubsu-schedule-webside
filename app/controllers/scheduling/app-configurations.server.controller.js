/**
 * Created by eugene on 29.04.15.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('../errors.server.controller'),
  AppConfiguration = mongoose.model('AppConfiguration'),
  Q = require('q'),
  _ = require('lodash');

/**
 * Create a configuration
 */

exports.create = function(req, res) {
  var configuration = new AppConfiguration(req.body);
  Q
    .try(function() {
      return Q.ninvoke(AppConfiguration.find({active: true}), 'exec');
    })
    .then(function(configurations) {
      if (configuration.active) {
        var promises = [];
        _.forEach(configurations, function(configuration) {
          promises.push(Q.try(function() {
            configuration.active = false;
            return Q.ninvoke(configuration, 'save');
          }));
        });
        return Q.all(promises);
      }
    })
    .then(function() {
      return Q.ninvoke(configuration, 'save');
    })
    .then(function() {
      res.json(configuration);
    })
    .catch(function(error) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(error)
      });
    });
};

/**
 * Show the current configuration
 */
exports.read = function(req, res) {
  res.json(req.configuration);
};

/**
 * Update a configuration
 */
exports.update = function(req, res) {
  var configuration = req.configuration;

  configuration = _.extend(configuration, req.body);
  Q
    .ninvoke(AppConfiguration.find({_id: configuration._id}), 'exec')
    .then(function(configurations) {
      if (!configuration.active) {
        if (configurations.length === 1 && configurations[0]._id === configuration._id) {
          throw new Error('Хотя бы одна конфигурация должна оставаться активной.');
        }
      } else {
        var promises = [];
        _.forEach(configurations, function(configuration) {
          promises.push(Q.try(function() {
            configuration.active = false;
            Q.ninvoke(configuration, 'save');
          }));
        });
        return Q.all(promises);
      }
    })
    .then(function() {
      Q.ninvoke(configuration, 'save');
    })
    .then(function() {
      res.json(configuration);
    })
    .catch(function(error) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(error)
      });
    });
};

/**
 * Delete an configuration
 */
exports.delete = function(req, res) {
  var configuration = req.configuration;

  if (configuration.active) {
    return res.status(400).send({
      message: 'Нельзя удалить активную конфигурацию.'
    });
  } else {
    configuration.remove(function(err) {
      if (err) {
        return res.status(400).send({
          message: errorHandler.getErrorMessage(err)
        });
      } else {
        res.json(configuration);
      }
    });
  }
};

/**
 * List of configurations
 */
exports.list = function(req, res) {
  AppConfiguration.find().exec(function(err, configurations) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(configurations);
    }
  });
};

/**
 * configuration middleware
 */
exports.configurationByID = function(req, res, next, id) {
  AppConfiguration.findById(id).exec(function(err, configuration) {
    if (err) return next(err);
    if (!configuration) return next(new Error('Failed to load configuration ' + id));
    req.configuration = configuration;
    next();
  });
};

/**
 * configuration authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
  if (!_.intersection(req.user.roles, ['admin']).length) {
    return res.status(403).send({
      message: 'User is not authorized'
    });
  }
  next();
};
