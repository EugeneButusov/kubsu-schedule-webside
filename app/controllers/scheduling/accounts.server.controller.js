/**
 * Created by eugene on 01.12.14.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('../errors.server.controller'),
    User = mongoose.model('User'),
    _ = require('lodash');


/**
 * Show the current user
 */
exports.read = function(req, res) {
    res.json(req.user);
};

/**
 * Update a user
 */
exports.update = function(req, res) {
    var user = req.user;

    user = _.extend(user, req.body);

    user.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(user);
        }
    });
};

/**
 * Delete an user
 */
exports.delete = function(req, res) {
    var user = req.user;

    user.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(user);
        }
    });
};

/**
 * List of users
 */
exports.list = function(req, res) {
    User.find(_.extend(req.query, req.body)).exec(function(err, users) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(users);
        }
    });
};

/**
 * user middleware
 */
exports.userByID = function(req, res, next, id) {
    User.findById(id).exec(function(err, user) {
        if (err) return next(err);
        if (!user) return next(new Error('Failed to load lesson ' + id));
        req.user = user;
        next();
    });
};

/**
 * Lesson authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    if (!_.intersection(req.user.roles, ['admin', 'editor']).length) {
        return res.status(403).send({
        message: 'User is not authorized'
     });
    }
    next();
};
