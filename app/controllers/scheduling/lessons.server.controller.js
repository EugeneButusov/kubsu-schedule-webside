/**
 * Created by eugene on 15.11.14.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('../errors.server.controller'),
    Lesson = mongoose.model('Lesson'),
    _ = require('lodash');

/**
 * Create a lesson
 */
exports.create = function(req, res) {
    var lesson = new Lesson(req.body);

    lesson.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(lesson);
        }
    });
};

/**
 * Show the current lesson
 */
exports.read = function(req, res) {
    res.json(req.lesson);
};

/**
 * Update a lesson
 */
exports.update = function(req, res) {
    var lesson = req.lesson;

    lesson = _.extend(lesson, req.body);

    lesson.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(lesson);
        }
    });
};

/**
 * Delete an lesson
 */
exports.delete = function(req, res) {
    var lesson = req.lesson;

    lesson.remove(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(lesson);
        }
    });
};

/**
 * List of Lessons
 */
exports.list = function(req, res) {
    Lesson.find().exec(function(err, lessons) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(lessons);
        }
    });
};

/**
 * Lesson middleware
 */
exports.lessonByID = function(req, res, next, id) {
    Lesson.findById(id).exec(function(err, lesson) {
        if (err) return next(err);
        if (!lesson) return next(new Error('Failed to load lesson ' + id));
        req.lesson = lesson;
        next();
    });
};

/**
 * Lesson authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    if (!req.user) {
        return res.status(403).send({
            message: 'User is not authorized'
        });
    }
    next();
};
