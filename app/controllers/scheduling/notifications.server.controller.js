/**
 * Created by eugene on 20.05.15.
 */

'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  errorHandler = require('../errors.server.controller'),
  Notification = mongoose.model('Notification'),
  _ = require('lodash'),
  Q = require('q');

/**
 * Create a notification
 */
exports.create = function(req, res) {
  req.body.author = req.user._id;
  Q
    .ninvoke(new Notification(req.body), 'save')
    .spread(function(notification) {
      res.json(notification);
    })
    .catch(function(error) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    });
};

/**
 * Show the current notification
 */
exports.read = function(req, res) {
  res.json(req.notification);
};

/**
 * Update a notification
 */
exports.update = function(req, res) {
  var notification = req.notification;

  notification = _.extend(notification, req.body);

  notification.save(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(notification);
    }
  });
};

/**
 * Delete an notification
 */
exports.delete = function(req, res) {
  var notification = req.notification;

  notification.remove(function(err) {
    if (err) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(notification);
    }
  });
};

/**
 * List of notification
 */

exports.list = function(req, res) {
  Q
    .ninvoke(Notification.find().populate('readUnions author'), 'exec')
    .then(function(notifications) {
      res.json(notifications);
    })
    .catch(function(error) {
      return res.status(400).send({
        message: errorHandler.getErrorMessage(error.message)
      });
    });
};

/**
 * notifications middleware
 */
exports.notificationByID = function(req, res, next, id) {
  Notification.findById(id).exec(function(err, notification) {
    if (err) return next(err);
    if (!notification) return next(new Error('Failed to load notification ' + id));
    req.notification = notification;
    next();
  });
};

/*** notification authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
  next();
};
