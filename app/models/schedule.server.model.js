/**
 * Created by eugene on 14.11.14.
 */

'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

//Schedule
var ScheduleSchema = new Schema({
  cyclePosition: {
    type: Number,
    required: true
  },
  subject: {
    type: Schema.Types.ObjectId,
    ref: 'Subject',
    required: true
  },
  type: {
    type: Schema.Types.ObjectId,
    ref: 'LessonType',
    required: true
  },
  lesson: {
    type: Schema.Types.ObjectId,
    ref: 'Lesson',
    required: true
  },
  playground: {
    type: Schema.Types.ObjectId,
    ref: 'Playground',
    required: true
  },
  teacher: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  learningUnions: [{
    type: Schema.Types.ObjectId,
    ref: 'Union'
  }]
});

mongoose.model('Schedule', ScheduleSchema);
