/**
 * Created by eugene on 04.04.15.
 */

'use strict';

var mongoose = require('mongoose'),
      Schema = mongoose.Schema;

//Application Configuration schema
var AppConfigurationSchema = new Schema({
  name: String,
  scheduleCycle: {
    length: Number,
    freeDays: [{
      type: Number
    }],
    startDate: Date
  },
  active: Boolean
});

mongoose.model('AppConfiguration', AppConfigurationSchema);
