/**
 * Created by eugene on 14.11.14.
 */

'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

//Subject of course
var SubjectSchema = new Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: String
});

mongoose.model('Subject', SubjectSchema);