/**
 * Created by eugene on 14.11.14.
 */

'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

//Places where lessons are completed
var PlaygroundSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }
});

mongoose.model('Playground', PlaygroundSchema);