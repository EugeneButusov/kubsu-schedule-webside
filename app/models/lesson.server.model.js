/**
 * Created by eugene on 14.11.14.
 */

'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

//Lesson schema
var LessonSchema = new Schema({
    number: {
        type: Number,
        default: 0
    },
    beginTime: {
        type: Date,
        default: Date.now
    },
    endTime: {
        type: Date,
        default: Date.now
    }
});

mongoose.model('Lesson', LessonSchema);