/**
 * Created by eugene on 23.11.14.
 */

'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

//Users Unions
var UnionSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    description: String,
    parent: {
        type: Schema.Types.ObjectId,
        ref: 'Union'
    }
});

mongoose.model('Union', UnionSchema);