/**
 * Created by eugene on 20.05.15.
 */

'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

// Notification
var NotificationSchema = new Schema({
  at: {
    type: Date,
    required: true,
    default: Date.now
  },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  title: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  readUnions: [{
    type: Schema.Types.ObjectId,
    ref: 'Union'
  }]
});

mongoose.model('Notification', NotificationSchema);
