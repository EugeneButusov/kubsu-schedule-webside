/**
 * Created by eugene on 29.04.15.
 */

'use strict';

var users = require('../../../app/controllers/users.server.controller'),
    configurations = require('../../controllers/scheduling/app-configurations.server.controller.js');

module.exports = function(app) {
  // configurations Routes
  app.route('/configurations')
    .get(configurations.list)
    .post(users.requiresLogin, configurations.create);

  app.route('/configurations/:configurationId')
    .get(configurations.read)
    .put(users.requiresLogin, configurations.hasAuthorization, configurations.update)
    .delete(users.requiresLogin, configurations.hasAuthorization, configurations.delete);

  // Finish by binding the lesson type middleware
  app.param('configurationId', configurations.configurationByID);
};
