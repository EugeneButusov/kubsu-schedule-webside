/**
 * Created by eugene on 28.11.14.
 */

'use strict';

var users = require('../../../app/controllers/users.server.controller'),
    lessonTypes = require('../../controllers/scheduling/lesson-types.server.controller.js');

module.exports = function(app) {
    // Lesson type Routes
    app.route('/lesson-types')
        .get(lessonTypes.list)
        .post(users.requiresLogin, lessonTypes.create);

    app.route('/lesson-types/:lessonTypeId')
        .get(lessonTypes.read)
        .put(users.requiresLogin, lessonTypes.hasAuthorization, lessonTypes.update)
        .delete(users.requiresLogin, lessonTypes.hasAuthorization, lessonTypes.delete);

    // Finish by binding the lesson type middleware
    app.param('lessonTypeId', lessonTypes.lessonTypeByID);
};
