/**
 * Created by eugene on 15.11.14.
 */

'use strict';

var users = require('../../../app/controllers/users.server.controller'),
    playgrounds = require('../../../app/controllers/scheduling/playgrounds.server.controller');

module.exports = function(app) {
    // Playgrounds Routes
    app.route('/playgrounds')
        .get(playgrounds.list)
        .post(users.requiresLogin, playgrounds.create);

    app.route('/playgrounds/:playgroundId')
        .get(playgrounds.read)
        .put(users.requiresLogin, playgrounds.hasAuthorization, playgrounds.update)
        .delete(users.requiresLogin, playgrounds.hasAuthorization, playgrounds.delete);

    // Finish by binding the article middleware
    app.param('playgroundId', playgrounds.playgroundByID);
};