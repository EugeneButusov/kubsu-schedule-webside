/**
 * Created by eugene on 24.11.14.
 */

'use strict';

var users = require('../../../app/controllers/users.server.controller'),
    subjects = require('../../../app/controllers/scheduling/subjects.server.controller');

module.exports = function(app) {
    // Subjects Routes
    app.route('/subjects')
        .get(subjects.list)
        .post(users.requiresLogin, subjects.create);

    app.route('/subjects/:subjectId')
        .get(subjects.read)
        .put(users.requiresLogin, subjects.hasAuthorization, subjects.update)
        .delete(users.requiresLogin, subjects.hasAuthorization, subjects.delete);

    // Finish by binding the article middleware
    app.param('subjectId', subjects.subjectByID);
};