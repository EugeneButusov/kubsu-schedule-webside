/**
 * Created by eugene on 15.11.14.
 */

'use strict';

var users = require('../../../app/controllers/users.server.controller'),
    lessons = require('../../../app/controllers/scheduling/lessons.server.controller');

module.exports = function(app) {
    // Lesson Routes
    app.route('/lessons')
        .get(users.requiresLogin, lessons.hasAuthorization, lessons.list)
        .post(users.requiresLogin, lessons.hasAuthorization, lessons.create);

    app.route('/lessons/:lessonId')
        .get(users.requiresLogin, lessons.hasAuthorization, lessons.read)
        .put(users.requiresLogin, lessons.hasAuthorization, lessons.update)
        .delete(users.requiresLogin, lessons.hasAuthorization, lessons.delete);

    // Finish by binding the article middleware
    app.param('lessonId', lessons.lessonByID);
};
