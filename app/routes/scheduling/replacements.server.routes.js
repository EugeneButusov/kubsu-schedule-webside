/**
 * Created by eugene on 20.05.15.
 */

/**
 * Created by eugene on 10.03.15.
 */

'use strict';

var users       = require('../../../app/controllers/users.server.controller'),
  replacements  = require('../../../app/controllers/scheduling/replacements.server.controller');

module.exports = function(app) {
  // replacements Routes
  app.route('/replacements')
    .get(replacements.list)
    .post(users.requiresLogin, replacements.create);

  app.route('/replacements/:replacementId')
    .get(replacements.read)
    .put(users.requiresLogin, replacements.hasAuthorization, replacements.update)
    .delete(users.requiresLogin, replacements.hasAuthorization, replacements.delete);

  app.param('replacementId', replacements.replacementByID);
};
