/**
 * Created by eugene on 10.03.15.
 */

'use strict';

var users       = require('../../../app/controllers/users.server.controller'),
    schedules   = require('../../../app/controllers/scheduling/schedules.server.controller');

module.exports = function(app) {
  // Subjects Routes
  app.route('/schedules')
    .get(schedules.list)
    .post(users.requiresLogin, schedules.create);

  app.route('/schedules/:scheduleId')
    .get(schedules.read)
    .put(users.requiresLogin, schedules.hasAuthorization, schedules.update)
    .delete(users.requiresLogin, schedules.hasAuthorization, schedules.delete);

  // Finish by binding the article middleware
  app.param('scheduleId', schedules.scheduleByID);
};
