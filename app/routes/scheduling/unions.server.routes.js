/**
 * Created by eugene on 23.11.14.
 */

'use strict';

var users = require('../../../app/controllers/users.server.controller'),
    unions = require('../../../app/controllers/scheduling/unions.server.controller');

module.exports = function(app) {
    // Unions Routes
    app.route('/unions')
        .get(unions.list)
        .post(users.requiresLogin, unions.create);

    app.route('/unions/:unionId')
        .get(unions.read)
        .put(users.requiresLogin, unions.hasAuthorization, unions.update)
        .delete(users.requiresLogin, unions.hasAuthorization, unions.delete);

    // Finish by binding the article middleware
    app.param('unionId', unions.unionByID);
};