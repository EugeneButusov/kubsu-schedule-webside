/**
 * Created by eugene on 01.12.14.
 */

'use strict';

var users = require('../../../app/controllers/users.server.controller'),
    accounts = require('../../../app/controllers/scheduling/accounts.server.controller');

module.exports = function(app) {
    // Lesson Routes
    app.route('/accounts')
        .get(users.requiresLogin, accounts.hasAuthorization, accounts.list);

    app.route('/accounts/:accountId')
        .get(users.requiresLogin, accounts.hasAuthorization, accounts.read)
        .put(users.requiresLogin, accounts.hasAuthorization, accounts.update);

    // Finish by binding the article middleware
    app.param('accountId', accounts.userByID);
};
