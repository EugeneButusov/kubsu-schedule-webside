/**
 * Created by eugene on 20.05.15.
 */

/**
 * Created by eugene on 20.05.15.
 */

/**
 * Created by eugene on 10.03.15.
 */

'use strict';

var users       = require('../../../app/controllers/users.server.controller'),
  notifications  = require('../../../app/controllers/scheduling/notifications.server.controller');

module.exports = function(app) {
  // notifications Routes
  app.route('/notifications')
    .get(notifications.list)
    .post(users.requiresLogin, notifications.create);

  app.route('/notifications/:notificationId')
    .get(notifications.read)
    .put(users.requiresLogin, notifications.hasAuthorization, notifications.update)
    .delete(users.requiresLogin, notifications.hasAuthorization, notifications.delete);

  app.param('notificationId', notifications.notificationByID);
};
