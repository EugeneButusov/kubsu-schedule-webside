'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
	LocalStrategy = require('passport-local').Strategy,
	User = require('mongoose').model('User');

module.exports = function() {
	// Use local strategy
	passport.use(new LocalStrategy({
			usernameField: 'username',
			passwordField: 'password'
		},
		function(username, password, done) {
			User.findOne({
				username: username
			}, function(err, user) {
				if (err) {
					return done(err);
				}
				if (!user) {
					return done(null, false, {
						message: 'Неизвестный пользователь или неверный пароль.'
					});
				}
				if (!user.authenticate(password)) {
					return done(null, false, {
						message: 'Неизвестный пользователь или неверный пароль.'
					});
				}
				if (!user.activated) {
					return done(null, false, {
						message: 'Аккаунт не активирован. Если активация не проиcходит продолжительное время - пожалуйста, свяжитесь с администратором системы.'
					});
				}

				return done(null, user);
			});
		}
	));
};
