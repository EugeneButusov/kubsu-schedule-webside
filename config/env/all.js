'use strict';

module.exports = {
	app: {
		title: 'КубГУ-расписание',
		description: 'Портал расписания занятий Кубанского Государственного Университета',
		keywords: 'mongodb, express, angularjs, node.js, mongoose, passport'
	},
	port: process.env.PORT || 3000,
	secure: process.env.SECURE || false,
	templateEngine: 'swig',
	sessionSecret: 'KUBSU',
	sessionCollection: 'sessions',
    log: {
        // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
        format: 'combined',
        // Stream defaults to process.stdout
        // Uncomment to enable logging to a log on the file system
        options: {
            stream: 'access.log'
        }
    },
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.css',
                'public/lib/angular-ui-tree/dist/*.css',
				'public/lib/angular-multi-select/*.css',
				'public/lib/bootstrap-daterangepicker/*.css'
			],
			js: [
				'public/lib/angular/angular.js',
                'public/lib/jquery/dist/*.js',
				'public/lib/angular-resource/angular-resource.js',
				'public/lib/angular-animate/angular-animate.js',
				'public/lib/angular-ui-router/release/angular-ui-router.js',
				'public/lib/angular-ui-utils/ui-utils.js',
                'public/lib/angular-ui-tree/dist/*.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.js',
                'public/lib/bootstrap/js/*.js',
				'public/lib/angular-multi-select/*.js',
				'public/lib/bootstrap-daterangepicker/*.js',
				'public/lib/angular-daterangepicker/js/*.js',
				'public/lib/ng-lodash/build/ng-lodash.min.js'
			]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js'
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};
