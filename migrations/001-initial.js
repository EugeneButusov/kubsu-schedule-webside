/**
 * Created by eugene on 15.11.14.
 */

'use strict';

require('../config/mongoose')();

  require('../app/models/lesson.server.model');
  require('../app/models/union.server.model');

var
    Q = require('q'),
    init = require('../config/init')(),
    Union = require('mongoose').model('Union'),
    log = require('../config/log')(module)
    ;

exports.up = function (next) {
    Q
        .try(function() {
            log.info("Create unions root item");
            var union = new Union({name: "Университет"});
            return Q.ninvoke(union, 'save');
        })
        .then(function () {
            next();
        })
        .catch(function (err) {
            log.error("Error", err);
            next(err);
        })
    ;
};

exports.down = function (next) {
    Q
        .try(function () {
            log.info("Remove default client");
            return Q.ninvoke(Client, 'remove', { clientId: 'default' });
        })
        .then(function () {
            next();
        })
        .catch(function (err) {
            log.error("Error", err);
            next(err);
        })
    ;
};
